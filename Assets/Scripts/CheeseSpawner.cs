﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;


public class CheeseSpawner : MonoBehaviour
{
    [SerializeField]
    GameObject CellPrefab;
    private Vector3 CellSize = new Vector3(1, 1, 1);
    private float x_random;
    private float y_random;
  
    void Start()
    {
     InvokeRepeating("CheeseSpawn", 0.0f, 30f);
    }        
    void CheeseSpawn()
    {
        System.Random rng = new System.Random();
        x_random = rng.Next(0,18);
        y_random = rng.Next(0,18);

        GameObject c = Instantiate
                    (CellPrefab, new Vector3(
                       x_random * CellSize.x+CellSize.x/2,
                       0.25f * CellSize.y ,
                       y_random * CellSize.z + CellSize.z/2),                  
                    Quaternion.identity);        
    }
}









