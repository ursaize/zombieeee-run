﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheeseScript : MonoBehaviour
{
    [SerializeField]
    int ScoreAdd = 1;
    [SerializeField]
    GameObject PickupEffect;
    private ZombieSpawner _spawn;
  
    private void Start()
    {
        _spawn = GameObject.FindGameObjectWithTag("ZombieSpawn").GetComponent<ZombieSpawner>();
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            Score.score += ScoreAdd;
            GameObject.FindGameObjectWithTag("Player").GetComponent<Score>().TextScore.text = Score.score.ToString();
            Instantiate(PickupEffect, transform.position, transform.rotation);
            Destroy(gameObject);
            HpPlayer.HP+=0.05f;
            if (Score.score % 10 == 0)
            {
                _spawn.ReTrue();
            }
        }
    }
}

