﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightActivate : MonoBehaviour
{
    private Light myLight;
    void Start()
    {
        myLight = GetComponent<Light>();
        InvokeRepeating("LightActivater", 0.0f, 5f);
    }
    void LightActivater()
    {
        myLight.enabled = !myLight.enabled;
      
    }
}
