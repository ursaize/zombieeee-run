﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGame : MonoBehaviour
{
    [SerializeField]
    int ScoreAdd = 10;
    [SerializeField]
    GameObject PickupEffect;
    [SerializeField]
    GameObject menuPanel; 
 
    void OnTriggerEnter(Collider col)
    {        
        if (col.tag == "Player")
        {
            Score.score += ScoreAdd;
            GameObject.FindGameObjectWithTag("Player").GetComponent<Score>().TextScore.text = Score.score.ToString();
            Instantiate(PickupEffect, transform.position, transform.rotation);
            menuPanel.SetActive(!menuPanel.activeSelf);
            Destroy(gameObject);
        }
    }
}
