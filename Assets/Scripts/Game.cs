﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    [SerializeField]
    GameObject text;
    [SerializeField]
    GameObject settings;
    [SerializeField]
    GameObject glory;
    [SerializeField]
    InputField field;
    private Result _result;

    public void StartGame() => Application.LoadLevel(1);
    public void ExitGame() => Application.Quit();
    public void MenuGame()
        {          
          Destroy(GameObject.FindGameObjectWithTag("Music"));
          Application.LoadLevel(0);
        }
    public void Settings()
    {       
        settings.SetActive(!settings.activeSelf);
        TextActivate();
    }
    public void Glory()
    {
        glory.SetActive(!glory.activeSelf);
        TextActivate();

        GameObject.FindGameObjectWithTag("Score").GetComponent<Result>().TextName.text = PlayerPrefs.GetString("PlayerName");
        GameObject.FindGameObjectWithTag("Score").GetComponent<Result>().TextScore.text = PlayerPrefs.GetInt("PlayerScore").ToString();
    }
    void TextActivate()
    {
        text.SetActive(!text.activeSelf);
    }
    public void CreateResult()
    { 
        int score = PlayerPrefs.GetInt("PlayerScore");
        if (score <= Score.score)
        {
            PlayerPrefs.SetInt("PlayerScore", Score.score);
            PlayerPrefs.SetString("PlayerName", field.text);
        }
        MenuGame();
    }
}