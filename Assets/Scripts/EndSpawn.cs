﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndSpawn : MonoBehaviour
{
    [SerializeField]
    GameObject CellPrefab;
    public static float xVector;
    public static float zVector;

    private Vector3 CellSize = new Vector3(1, 1, 1);
       
    void Start()
    {
        GameObject c = Instantiate
            (CellPrefab, new Vector3(
               xVector * CellSize.x + CellSize.x / 2,
               0.5f * CellSize.y,
               zVector * CellSize.z + CellSize.z / 2),

            Quaternion.identity);
    }
}
