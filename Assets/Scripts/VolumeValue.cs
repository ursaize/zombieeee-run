﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;


public class VolumeValue : MonoBehaviour
{
    [SerializeField]
    Slider slider;
    
    void Start()
    {
        if (!PlayerPrefs.HasKey("volume")) slider.value = 1;
        slider.value = PlayerPrefs.GetFloat("volume");
    }
    void Update()
    {
        PlayerPrefs.SetFloat("volume", slider.value);
        if (PlayerPrefs.GetInt("Muted", 0) == 0)
        {
            AudioListener.volume = slider.value;
        }
        
    }
}
