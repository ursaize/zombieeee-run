﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCam : MonoBehaviour
{
    
    [SerializeField] Transform target;
    [SerializeField] Vector3 defaultDistance = new Vector3(0f, 25f, 0f);
    [SerializeField] float distanceDamp = 0f;//возможна модификация
        
    public Vector3 velocity = Vector3.one;

    public void Start()
    {
        if (PlayerPrefs.GetInt("Muted", 0) == 0)
        {
            AudioListener.volume = PlayerPrefs.GetFloat("volume");
        }
    }
    private void Update()
    {        
        transform.position = Vector3.SmoothDamp(target.position, target.position+defaultDistance, ref velocity, distanceDamp);
    }
}
