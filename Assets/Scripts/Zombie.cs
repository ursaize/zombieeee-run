﻿
using UnityEngine;


public class Zombie : MonoBehaviour
{
    [SerializeField]
    public float speed = 1f;

    private static Animator anim;
    private Transform player;
    private Rigidbody zombie;
    
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        anim = GetComponent<Animator>();
        zombie = GetComponent<Rigidbody>();
        gameObject.GetComponent<Animator>().SetBool("isIdle", true);
    }
    void OnTriggerStay(Collider col)
    {
        if (col.tag == "Player")
        {
            Vector3 direction = player.position - this.transform.position;
            direction.y = 0;

            this.transform.rotation =
            Quaternion.Slerp(this.transform.rotation,
            Quaternion.LookRotation(direction), 0.1f);
            Vector3 velocity = direction * speed;
            
            anim.SetBool("isIdle", false);

            if (direction.magnitude > 0.5)
            {
                zombie.velocity = new Vector3(velocity.x, zombie.velocity.y, velocity.z);
                anim.SetBool("isWalking", true);
                anim.SetBool("isAttacking", false);
            }
            else
            {
                anim.SetBool("isAttacking", true);
                anim.SetBool("isWalking", false);
            }
        }
    }
    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            anim.SetBool("isWalking", false);
            anim.SetBool("isIdle", true);
        }
    }
}
