﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieSpawner : MonoBehaviour
{
    [SerializeField]
    GameObject CellPrefab;
    private Vector3 CellSize = new Vector3(1, 1, 1);
    private float x_random;
    private float y_random;
    private static bool _createZombie = true;

    void Start()
    {
        ZombieSpawn();
    }
    void Update()
    {       
       if ((Score.score % 10 == 0 )
            && (Score.score!=0 )
            && (_createZombie==true))
       {            
            ZombieSpawn();
            _createZombie = false;
       }
    }
    public void ReTrue()
    {
        _createZombie = true;
    }
    void ZombieSpawn()
    {
        System.Random rng = new System.Random();
            x_random = rng.Next(0, 18);
            y_random = rng.Next(0, 18);

            GameObject c = Instantiate
                        (CellPrefab, new Vector3(
                           x_random * CellSize.x + CellSize.x / 2,
                           0.05f * CellSize.y,
                           y_random * CellSize.z + CellSize.z / 2),
                        Quaternion.identity);      
    }
}
