﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LabyrinthSpawner : MonoBehaviour
{
    [SerializeField]
    Cell CellPrefab;
    [SerializeField]
    Vector3 CellSize = new Vector3(1, 1, 1); 

    private void Start()
    {
        LabyrinthGenerator generator = new LabyrinthGenerator();
        LabyrinthGeneratorCell[,] labyr = generator.GenerateLabyrinth();

        for(int x=0;x<labyr.GetLength(0);x++)
        {
            for(int y=0;y<labyr.GetLength(1);y++)
            {
                Cell c = Instantiate
                    (CellPrefab, new Vector3
                    (x * CellSize.x, 
                    y * CellSize.y,
                    y * CellSize.z),
                    Quaternion.identity);

                if (x == (labyr.GetLength(0) - 1))
                    c.Floor.SetActive(!labyr[x,y].Floor);
                if (y == (labyr.GetLength(1) - 1))
                    c.Floor.SetActive(!labyr[x,y].Floor);

                c.WallLeft.SetActive(labyr[x, y].WallLeft);
                c.WallBottom.SetActive(labyr[x, y].WallBottom);
            }
        }     
    }
}
