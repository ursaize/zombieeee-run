﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HpPlayer : MonoBehaviour
{
    [SerializeField]
    Image UIHP;
    public static float HP = 1f;
    private Animator ch_animator;
    [SerializeField]
    GameObject menuPanel;
       
    void Start()
    {
        HP = 1f;
        ch_animator = GetComponent<Animator>();

    }
    void Update()
    {
       
        UIHP.fillAmount = HP;   
    }
    void OnTriggerStay (Collider other)
    {
        if(other.tag=="ZombieDamage")
        {
            HP -= Time.deltaTime*3;
            if (HP <= 0)
            {
                StartCoroutine(Death());
            }
        } 
    }
    IEnumerator Death()
    {
        GetComponent<PlayerController>().enabled = false;
        ch_animator.SetBool("Death", true);
        yield return new WaitForSeconds(2f);
        menuPanel.SetActive(!menuPanel.activeSelf);
        Destroy(gameObject);
    }

}
