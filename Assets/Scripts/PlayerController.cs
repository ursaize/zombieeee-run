﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour
{
    [SerializeField]
    float SpeedMove;
    [SerializeField]
    Vector3 moveVector;

    private CharacterController ch_controller;
    private Animator ch_animator;
    private MobileController mContr;

    private void Start()
    {
        ch_controller = GetComponent<CharacterController>();
        ch_animator = GetComponent<Animator>();
        mContr = GameObject.FindGameObjectWithTag("Joystick").GetComponent<MobileController>();
    }
    private void Update()
    {
        CharacterMove();
    }
    private void CharacterMove()
    {
        moveVector = Vector3.zero;
        moveVector.x = mContr.Horizontal() * SpeedMove;
        moveVector.z = mContr.Vertical() * SpeedMove;

        if (moveVector.x != 0 || moveVector.z != 0) ch_animator.SetBool("Move", true);
        else ch_animator.SetBool("Move", false);

        if(Vector3.Angle(Vector3.forward,moveVector)>1f|| Vector3.Angle(Vector3.forward, moveVector) == 0)//на случай добавления прыжков
        {
            Vector3 direct = Vector3.RotateTowards(transform.forward, moveVector, SpeedMove, 0.0f);
            transform.rotation = Quaternion.LookRotation(direct);
        }
        ch_controller.Move(moveVector * Time.deltaTime);
    }
}
