﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 public class Labyrinth
    {
        LabyrinthGeneratorCell[,] cells; 
    }
 public class LabyrinthGeneratorCell
    {
        public int X;
        public int Y;

        public bool WallLeft = true; 
        public bool WallBottom = true;
        public bool Floor = true;

        public bool Visited = false;
        public int DistanceFromStart;
    }

